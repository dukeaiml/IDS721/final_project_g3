import sys
import json
from transformers import AutoModelForSequenceClassification, AutoTokenizer

try:
    tokenizer = AutoTokenizer.from_pretrained("nlptown/bert-base-multilingual-uncased-sentiment")
    model = AutoModelForSequenceClassification.from_pretrained("nlptown/bert-base-multilingual-uncased-sentiment")

    def get_sentiment(text):
        inputs = tokenizer(text, return_tensors="pt", truncation=True, padding=True)
        outputs = model(**inputs)
        prediction = outputs.logits.argmax(-1).item()

        sentiment_labels = {
            0: "Very Negative",
            1: "Negative",
            2: "Neutral",
            3: "Positive",
            4: "Very Positive"
        }
        return sentiment_labels[prediction]

    if __name__ == "__main__":
        input_text = sys.argv[1]
        result = get_sentiment(input_text)
        print(json.dumps({"response": result}))

except Exception as e:
    print(json.dumps({"error": "Failed to execute model", "details": str(e)}))
