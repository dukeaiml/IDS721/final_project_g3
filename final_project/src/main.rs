use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use std::process::Command;
use serde_json::{Value, json};  // ensure serde_json is included in Cargo.toml

async fn get_prediction(prompt: web::Json<String>) -> impl Responder {
    let output = Command::new("python")  // make sure 'python' points to the correct interpreter
        .arg("/usr/local/bin/llama_inference.py")
        .arg(prompt.into_inner())
        .output()
        .expect("Failed to execute command");

    if !output.stderr.is_empty() {
        eprintln!("Python STDERR: {}", String::from_utf8_lossy(&output.stderr));
    }

    let output_str = String::from_utf8_lossy(&output.stdout);
    match serde_json::from_str::<Value>(&output_str) {
        Ok(json) => HttpResponse::Ok().json(json),
        Err(e) => {
            eprintln!("Failed to parse JSON: {}", e);
            HttpResponse::BadRequest().json(json!({"error": "Failed to parse response", "details": e.to_string()}))
        }
    }
}



#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Starting server on http://0.0.0.0:8080");
    HttpServer::new(|| {
        App::new()
            .route("/predict", web::post().to(get_prediction))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
