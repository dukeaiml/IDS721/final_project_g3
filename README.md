# IDS Final Project

- Our team aims tom develop a project which uses LLM as a backend model through a web service developed in Rust. This involves containerizing the service for deployment on Kubernetes and automating the workflow with a CI/CD pipeline.

# DNS

- link: http://ad4de1ed163b9493db6efdffb11ca42f-1260656315.us-east-1.elb.amazonaws.com/predict
- If you want to test our functionality, you can use

```
curl -X POST http://ad4de1ed163b9493db6efdffb11ca42f-1260656315.us-east-1.elb.amazonaws.com/predict \
-H "Content-Type: application/json" \
-d '{"content": "I love this new movie!"}'
```

# Demo video

- [Demo Youtube Video](https://www.youtube.com/watch?v=_uHcLrpAQqM)

# How to build this project

- We need to set up a cargo project locally `cargo new <project_name>`
- In the sr/main.rs implemented the code to use Rust-Bert as an NLP pipelines for future usage.
- In the dockerfile, to run the nlp models to help us analyze the input text and give feedback.
- To test locally:
  1. `cd actix-container`
  2. `cargo run` to see the localhost running on the laptop
- To build the Docker Image:
  1. `docker build -t transformer .`
  2. `docker run -p 8080:8080 transformer`

# Deploy on AWS

- This project can be deployed on AWS by using AWS ECR and EKS. The ECR is to store the docker image that we built. This is somewhat similar to the miniproject_10. And EKS is more helpful to deploy the functions so that we can get a DNS address.
- First we need to set up a public AWS ECR repo to create an instance.
- ![All text](screenshots/ec2.jpg)

- Then we need to build the docker image and upload the image to the above instance
- There are four steps in total:

  1. ` aws ecr get-login-password --region <primary region> | docker login --username AWS --password-stdin <repo-id>.dkr.ecr.us-east-1.amazonaws.com` to login into the aws
  2. `docker build -t <model> .` to build the docker image
  3. `docker tag transformer:latest <repo-id>.dkr.ecr.us-east-1.amazonaws.com/transformer:latest` to tag the image.
  4. `docker push <repo-id>.dkr.ecr.us-east-1.amazonaws.com/transformer:latest` to push the image to our ECR instance.

- We also need to eksctl and kubectl to help us configure the kubernetes.
- `eksctl create cluster --name <cluster name> --region <cluster region>` to first build the cluster on AW
- `kubectl create namespace <app-name>` because Namespaces provide a way to organize and segregate resources within a Kubernetes cluster. They are commonly used to create logical partitions within a cluster, allowing teams or applications to have their own isolated environment.
- `kubectl apply -f eks-sample-deployment.yaml`
  `kubectl apply -f eks-sample-service.yaml` to deploy our app to the cluster
- ![All text](screenshots/eks.jpg)

- we also set up the loadbalancer to scale out our sever
- ![All text](screenshots/loadbalancer.jpg)

## CI/CD Pipeline

- We have three stages for our pipeline, which are testing, building, deployment.

1. In the first stage, testing, we checked the format of our rust code for readbility.
2. In the second stage, building, we first ran

```
    - pip install awscli
    - aws configure set aws_access_key_id "$AWS_ACCESS_KEY_ID"
    - aws configure set aws_secret_access_key "$AWS_SECRET_ACCESS_KEY"
    - aws configure set default.region us-east-1
```

so that it correctly configures our AWS IAM user configuration and credentials. The variables `$AWS_ACCESS_KEY_ID`, `$AWS_SECRET_ACCESS_KEY` is stored in the gitlab CI/CD settings.

Then, we use the following command build docker and push to the public AWS ECR:

```
    - docker build --progress=plain -f ./$PROJECT_DIR/Dockerfile -t sentiment-service:latest ./$PROJECT_DIR
    - docker tag sentiment-service:latest public.ecr.aws/u6n4z2s9/transformer:latest
    - docker push public.ecr.aws/u6n4z2s9/transformer:latest
```

3. In the third stage, deployment, we applied the kubernetes deployment and service to our AWS EKS so that it uses the built docker image from the second stage for EC2 instances and load balancer.

- ![All text](screenshots/cicd.png)

## Test our functions

- We can go to postman and send a `POST` request with the body of

```
{
    "content" = "xxxxx"
}
```

- we will get the result from the nlp models.

- The following are our sample tests
- ![All text](screenshots/test1.png)
  ![All text](screenshots/test2.png)
# We also enabled monitor and logging:
- We used the following command to enable Kubernete logs in AWS EKS. Then we can go to CloudWatch to see the logs. 
```
eksctl utils update-cluster-logging --enable-types all --cluster my-cluster --region us-east-1 --approve
```
  ![All text](screenshots/logging.jpg)
- Also, we can monitor the HTTP requests through the api endpoint in the EC2 load balancer monitor tab. 
  ![All text](screenshots/metrics.jpeg)
