use actix_web::{web, HttpResponse, Responder};
use rust_bert::pipelines::sentiment::SentimentModel;
use serde::{Deserialize, Serialize};
use std::sync::{Mutex, MutexGuard};

// Define the Shared Data model directly here
pub struct SharedData {
    pub model: SentimentModel,
}

// Text input and output response definitions
#[derive(Deserialize)]
pub struct Text {
    pub content: String,
}

#[derive(Serialize)]
pub struct SentimentResponse {
    pub sentiment: String,
    pub details: String,
}

pub async fn put_file(data: web::Data<Mutex<SharedData>>, form: web::Json<Text>) -> impl Responder {
    let vs: MutexGuard<'_, SharedData> = data.lock().unwrap();
    let input = [form.content.as_ref()];
    let prediction = vs.model.predict(&input);

    let response = SentimentResponse {
        sentiment: format!("{:?}", prediction[0].polarity),
        details: format!("{:?}", prediction),
    };

    HttpResponse::Ok().json(response)
}
