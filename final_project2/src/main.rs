use actix_web::{
    web::{self},
    App, HttpServer,
};

// Import the put_file and SharedData directly here; no need to repeat them
mod handler;
use crate::handler::{put_file, SharedData};
use actix_web_prom::PrometheusMetricsBuilder;
use prometheus::{opts, IntCounterVec};
use rust_bert::pipelines::common::ModelResource;
use rust_bert::pipelines::common::ModelType::FNet;
use rust_bert::pipelines::sentiment::{SentimentConfig, SentimentModel};
use rust_bert::resources::LocalResource;
use std::path::PathBuf;
use std::sync::Mutex;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();

    let prometheus = PrometheusMetricsBuilder::new("api")
        .endpoint("/metrics")
        .build()
        .unwrap();

    let config_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/transformer_files/config.json"),
    });
    let vocab_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/transformer_files/spiece.model"),
    });
    let model_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/transformer_files/rust_model.ot"),
    });

    let model_config: SentimentConfig = SentimentConfig {
        model_type: FNet,
        model_resource: ModelResource::Torch(model_resource),
        vocab_resource,
        config_resource,
        ..Default::default()
    };

    let model = SentimentModel::new(model_config).unwrap();

    let data: web::Data<Mutex<SharedData>> = web::Data::new(Mutex::new(SharedData { model }));

    HttpServer::new(move || {
        App::new()
            .wrap(prometheus.clone())
            .app_data(data.clone())
            .route("/predict", web::post().to(put_file))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
